//
// Generated file, do not edit! Created by opp_msgtool 6.0 from LoRa/LoRaMacFrame.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wshadow"
#  pragma clang diagnostic ignored "-Wconversion"
#  pragma clang diagnostic ignored "-Wunused-parameter"
#  pragma clang diagnostic ignored "-Wc++98-compat"
#  pragma clang diagnostic ignored "-Wunreachable-code-break"
#  pragma clang diagnostic ignored "-Wold-style-cast"
#elif defined(__GNUC__)
#  pragma GCC diagnostic ignored "-Wshadow"
#  pragma GCC diagnostic ignored "-Wconversion"
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  pragma GCC diagnostic ignored "-Wold-style-cast"
#  pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn"
#  pragma GCC diagnostic ignored "-Wfloat-conversion"
#endif

#include <iostream>
#include <sstream>
#include <memory>
#include <type_traits>
#include "LoRaMacFrame_m.h"

namespace omnetpp {

// Template pack/unpack rules. They are declared *after* a1l type-specific pack functions for multiple reasons.
// They are in the omnetpp namespace, to allow them to be found by argument-dependent lookup via the cCommBuffer argument

// Packing/unpacking an std::vector
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::vector<T,A>& v)
{
    int n = v.size();
    doParsimPacking(buffer, n);
    for (int i = 0; i < n; i++)
        doParsimPacking(buffer, v[i]);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::vector<T,A>& v)
{
    int n;
    doParsimUnpacking(buffer, n);
    v.resize(n);
    for (int i = 0; i < n; i++)
        doParsimUnpacking(buffer, v[i]);
}

// Packing/unpacking an std::list
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::list<T,A>& l)
{
    doParsimPacking(buffer, (int)l.size());
    for (typename std::list<T,A>::const_iterator it = l.begin(); it != l.end(); ++it)
        doParsimPacking(buffer, (T&)*it);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::list<T,A>& l)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        l.push_back(T());
        doParsimUnpacking(buffer, l.back());
    }
}

// Packing/unpacking an std::set
template<typename T, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::set<T,Tr,A>& s)
{
    doParsimPacking(buffer, (int)s.size());
    for (typename std::set<T,Tr,A>::const_iterator it = s.begin(); it != s.end(); ++it)
        doParsimPacking(buffer, *it);
}

template<typename T, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::set<T,Tr,A>& s)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        T x;
        doParsimUnpacking(buffer, x);
        s.insert(x);
    }
}

// Packing/unpacking an std::map
template<typename K, typename V, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::map<K,V,Tr,A>& m)
{
    doParsimPacking(buffer, (int)m.size());
    for (typename std::map<K,V,Tr,A>::const_iterator it = m.begin(); it != m.end(); ++it) {
        doParsimPacking(buffer, it->first);
        doParsimPacking(buffer, it->second);
    }
}

template<typename K, typename V, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::map<K,V,Tr,A>& m)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        K k; V v;
        doParsimUnpacking(buffer, k);
        doParsimUnpacking(buffer, v);
        m[k] = v;
    }
}

// Default pack/unpack function for arrays
template<typename T>
void doParsimArrayPacking(omnetpp::cCommBuffer *b, const T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimPacking(b, t[i]);
}

template<typename T>
void doParsimArrayUnpacking(omnetpp::cCommBuffer *b, T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimUnpacking(b, t[i]);
}

// Default rule to prevent compiler from choosing base class' doParsimPacking() function
template<typename T>
void doParsimPacking(omnetpp::cCommBuffer *, const T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimPacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

template<typename T>
void doParsimUnpacking(omnetpp::cCommBuffer *, T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimUnpacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

}  // namespace omnetpp

namespace flora {

Register_Enum(flora::MacFrameType, (flora::MacFrameType::OTHER, flora::MacFrameType::BEACON, flora::MacFrameType::UPLINK, flora::MacFrameType::DOWNLINK));

Register_Class(LoRaMacFrame)

LoRaMacFrame::LoRaMacFrame() : ::inet::FieldsChunk()
{
}

LoRaMacFrame::LoRaMacFrame(const LoRaMacFrame& other) : ::inet::FieldsChunk(other)
{
    copy(other);
}

LoRaMacFrame::~LoRaMacFrame()
{
    delete [] this->route;
    delete [] this->timestamps;
}

LoRaMacFrame& LoRaMacFrame::operator=(const LoRaMacFrame& other)
{
    if (this == &other) return *this;
    ::inet::FieldsChunk::operator=(other);
    copy(other);
    return *this;
}

void LoRaMacFrame::copy(const LoRaMacFrame& other)
{
    this->transmitterAddress = other.transmitterAddress;
    this->receiverAddress = other.receiverAddress;
    this->pktType = other.pktType;
    this->sequenceNumber = other.sequenceNumber;
    this->LoRaTP = other.LoRaTP;
    this->LoRaCF = other.LoRaCF;
    this->LoRaSF = other.LoRaSF;
    this->LoRaBW = other.LoRaBW;
    this->LoRaCR = other.LoRaCR;
    this->LoRaUseHeader = other.LoRaUseHeader;
    this->RSSI = other.RSSI;
    this->SNIR = other.SNIR;
    this->BeaconTimer = other.BeaconTimer;
    this->PingNb = other.PingNb;
    this->PingPeriod = other.PingPeriod;
    this->PingOffset = other.PingOffset;
    this->BeaconTime = other.BeaconTime;
    this->numHop = other.numHop;
    this->satNumber = other.satNumber;
    this->originTime = other.originTime;
    this->groundTime = other.groundTime;
    delete [] this->route;
    this->route = (other.route_arraysize==0) ? nullptr : new int[other.route_arraysize];
    route_arraysize = other.route_arraysize;
    for (size_t i = 0; i < route_arraysize; i++) {
        this->route[i] = other.route[i];
    }
    delete [] this->timestamps;
    this->timestamps = (other.timestamps_arraysize==0) ? nullptr : new ::omnetpp::simtime_t[other.timestamps_arraysize];
    timestamps_arraysize = other.timestamps_arraysize;
    for (size_t i = 0; i < timestamps_arraysize; i++) {
        this->timestamps[i] = other.timestamps[i];
    }
}

void LoRaMacFrame::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::inet::FieldsChunk::parsimPack(b);
    doParsimPacking(b,this->transmitterAddress);
    doParsimPacking(b,this->receiverAddress);
    doParsimPacking(b,this->pktType);
    doParsimPacking(b,this->sequenceNumber);
    doParsimPacking(b,this->LoRaTP);
    doParsimPacking(b,this->LoRaCF);
    doParsimPacking(b,this->LoRaSF);
    doParsimPacking(b,this->LoRaBW);
    doParsimPacking(b,this->LoRaCR);
    doParsimPacking(b,this->LoRaUseHeader);
    doParsimPacking(b,this->RSSI);
    doParsimPacking(b,this->SNIR);
    doParsimPacking(b,this->BeaconTimer);
    doParsimPacking(b,this->PingNb);
    doParsimPacking(b,this->PingPeriod);
    doParsimPacking(b,this->PingOffset);
    doParsimPacking(b,this->BeaconTime);
    doParsimPacking(b,this->numHop);
    doParsimPacking(b,this->satNumber);
    doParsimPacking(b,this->originTime);
    doParsimPacking(b,this->groundTime);
    b->pack(route_arraysize);
    doParsimArrayPacking(b,this->route,route_arraysize);
    b->pack(timestamps_arraysize);
    doParsimArrayPacking(b,this->timestamps,timestamps_arraysize);
}

void LoRaMacFrame::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::inet::FieldsChunk::parsimUnpack(b);
    doParsimUnpacking(b,this->transmitterAddress);
    doParsimUnpacking(b,this->receiverAddress);
    doParsimUnpacking(b,this->pktType);
    doParsimUnpacking(b,this->sequenceNumber);
    doParsimUnpacking(b,this->LoRaTP);
    doParsimUnpacking(b,this->LoRaCF);
    doParsimUnpacking(b,this->LoRaSF);
    doParsimUnpacking(b,this->LoRaBW);
    doParsimUnpacking(b,this->LoRaCR);
    doParsimUnpacking(b,this->LoRaUseHeader);
    doParsimUnpacking(b,this->RSSI);
    doParsimUnpacking(b,this->SNIR);
    doParsimUnpacking(b,this->BeaconTimer);
    doParsimUnpacking(b,this->PingNb);
    doParsimUnpacking(b,this->PingPeriod);
    doParsimUnpacking(b,this->PingOffset);
    doParsimUnpacking(b,this->BeaconTime);
    doParsimUnpacking(b,this->numHop);
    doParsimUnpacking(b,this->satNumber);
    doParsimUnpacking(b,this->originTime);
    doParsimUnpacking(b,this->groundTime);
    delete [] this->route;
    b->unpack(route_arraysize);
    if (route_arraysize == 0) {
        this->route = nullptr;
    } else {
        this->route = new int[route_arraysize];
        doParsimArrayUnpacking(b,this->route,route_arraysize);
    }
    delete [] this->timestamps;
    b->unpack(timestamps_arraysize);
    if (timestamps_arraysize == 0) {
        this->timestamps = nullptr;
    } else {
        this->timestamps = new ::omnetpp::simtime_t[timestamps_arraysize];
        doParsimArrayUnpacking(b,this->timestamps,timestamps_arraysize);
    }
}

const ::inet::MacAddress& LoRaMacFrame::getTransmitterAddress() const
{
    return this->transmitterAddress;
}

void LoRaMacFrame::setTransmitterAddress(const ::inet::MacAddress& transmitterAddress)
{
    handleChange();
    this->transmitterAddress = transmitterAddress;
}

const ::inet::MacAddress& LoRaMacFrame::getReceiverAddress() const
{
    return this->receiverAddress;
}

void LoRaMacFrame::setReceiverAddress(const ::inet::MacAddress& receiverAddress)
{
    handleChange();
    this->receiverAddress = receiverAddress;
}

int LoRaMacFrame::getPktType() const
{
    return this->pktType;
}

void LoRaMacFrame::setPktType(int pktType)
{
    handleChange();
    this->pktType = pktType;
}

int LoRaMacFrame::getSequenceNumber() const
{
    return this->sequenceNumber;
}

void LoRaMacFrame::setSequenceNumber(int sequenceNumber)
{
    handleChange();
    this->sequenceNumber = sequenceNumber;
}

double LoRaMacFrame::getLoRaTP() const
{
    return this->LoRaTP;
}

void LoRaMacFrame::setLoRaTP(double LoRaTP)
{
    handleChange();
    this->LoRaTP = LoRaTP;
}

::inet::Hz LoRaMacFrame::getLoRaCF() const
{
    return this->LoRaCF;
}

void LoRaMacFrame::setLoRaCF(::inet::Hz LoRaCF)
{
    handleChange();
    this->LoRaCF = LoRaCF;
}

int LoRaMacFrame::getLoRaSF() const
{
    return this->LoRaSF;
}

void LoRaMacFrame::setLoRaSF(int LoRaSF)
{
    handleChange();
    this->LoRaSF = LoRaSF;
}

::inet::Hz LoRaMacFrame::getLoRaBW() const
{
    return this->LoRaBW;
}

void LoRaMacFrame::setLoRaBW(::inet::Hz LoRaBW)
{
    handleChange();
    this->LoRaBW = LoRaBW;
}

int LoRaMacFrame::getLoRaCR() const
{
    return this->LoRaCR;
}

void LoRaMacFrame::setLoRaCR(int LoRaCR)
{
    handleChange();
    this->LoRaCR = LoRaCR;
}

bool LoRaMacFrame::getLoRaUseHeader() const
{
    return this->LoRaUseHeader;
}

void LoRaMacFrame::setLoRaUseHeader(bool LoRaUseHeader)
{
    handleChange();
    this->LoRaUseHeader = LoRaUseHeader;
}

double LoRaMacFrame::getRSSI() const
{
    return this->RSSI;
}

void LoRaMacFrame::setRSSI(double RSSI)
{
    handleChange();
    this->RSSI = RSSI;
}

double LoRaMacFrame::getSNIR() const
{
    return this->SNIR;
}

void LoRaMacFrame::setSNIR(double SNIR)
{
    handleChange();
    this->SNIR = SNIR;
}

int LoRaMacFrame::getBeaconTimer() const
{
    return this->BeaconTimer;
}

void LoRaMacFrame::setBeaconTimer(int BeaconTimer)
{
    handleChange();
    this->BeaconTimer = BeaconTimer;
}

int LoRaMacFrame::getPingNb() const
{
    return this->PingNb;
}

void LoRaMacFrame::setPingNb(int PingNb)
{
    handleChange();
    this->PingNb = PingNb;
}

int LoRaMacFrame::getPingPeriod() const
{
    return this->PingPeriod;
}

void LoRaMacFrame::setPingPeriod(int PingPeriod)
{
    handleChange();
    this->PingPeriod = PingPeriod;
}

int LoRaMacFrame::getPingOffset() const
{
    return this->PingOffset;
}

void LoRaMacFrame::setPingOffset(int PingOffset)
{
    handleChange();
    this->PingOffset = PingOffset;
}

int LoRaMacFrame::getBeaconTime() const
{
    return this->BeaconTime;
}

void LoRaMacFrame::setBeaconTime(int BeaconTime)
{
    handleChange();
    this->BeaconTime = BeaconTime;
}

int LoRaMacFrame::getNumHop() const
{
    return this->numHop;
}

void LoRaMacFrame::setNumHop(int numHop)
{
    handleChange();
    this->numHop = numHop;
}

int LoRaMacFrame::getSatNumber() const
{
    return this->satNumber;
}

void LoRaMacFrame::setSatNumber(int satNumber)
{
    handleChange();
    this->satNumber = satNumber;
}

::omnetpp::simtime_t LoRaMacFrame::getOriginTime() const
{
    return this->originTime;
}

void LoRaMacFrame::setOriginTime(::omnetpp::simtime_t originTime)
{
    handleChange();
    this->originTime = originTime;
}

::omnetpp::simtime_t LoRaMacFrame::getGroundTime() const
{
    return this->groundTime;
}

void LoRaMacFrame::setGroundTime(::omnetpp::simtime_t groundTime)
{
    handleChange();
    this->groundTime = groundTime;
}

size_t LoRaMacFrame::getRouteArraySize() const
{
    return route_arraysize;
}

int LoRaMacFrame::getRoute(size_t k) const
{
    if (k >= route_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)route_arraysize, (unsigned long)k);
    return this->route[k];
}

void LoRaMacFrame::setRouteArraySize(size_t newSize)
{
    handleChange();
    int *route2 = (newSize==0) ? nullptr : new int[newSize];
    size_t minSize = route_arraysize < newSize ? route_arraysize : newSize;
    for (size_t i = 0; i < minSize; i++)
        route2[i] = this->route[i];
    for (size_t i = minSize; i < newSize; i++)
        route2[i] = 0;
    delete [] this->route;
    this->route = route2;
    route_arraysize = newSize;
}

void LoRaMacFrame::setRoute(size_t k, int route)
{
    if (k >= route_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)route_arraysize, (unsigned long)k);
    handleChange();
    this->route[k] = route;
}

void LoRaMacFrame::insertRoute(size_t k, int route)
{
    if (k > route_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)route_arraysize, (unsigned long)k);
    handleChange();
    size_t newSize = route_arraysize + 1;
    int *route2 = new int[newSize];
    size_t i;
    for (i = 0; i < k; i++)
        route2[i] = this->route[i];
    route2[k] = route;
    for (i = k + 1; i < newSize; i++)
        route2[i] = this->route[i-1];
    delete [] this->route;
    this->route = route2;
    route_arraysize = newSize;
}

void LoRaMacFrame::appendRoute(int route)
{
    insertRoute(route_arraysize, route);
}

void LoRaMacFrame::eraseRoute(size_t k)
{
    if (k >= route_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)route_arraysize, (unsigned long)k);
    handleChange();
    size_t newSize = route_arraysize - 1;
    int *route2 = (newSize == 0) ? nullptr : new int[newSize];
    size_t i;
    for (i = 0; i < k; i++)
        route2[i] = this->route[i];
    for (i = k; i < newSize; i++)
        route2[i] = this->route[i+1];
    delete [] this->route;
    this->route = route2;
    route_arraysize = newSize;
}

size_t LoRaMacFrame::getTimestampsArraySize() const
{
    return timestamps_arraysize;
}

::omnetpp::simtime_t LoRaMacFrame::getTimestamps(size_t k) const
{
    if (k >= timestamps_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)timestamps_arraysize, (unsigned long)k);
    return this->timestamps[k];
}

void LoRaMacFrame::setTimestampsArraySize(size_t newSize)
{
    handleChange();
    ::omnetpp::simtime_t *timestamps2 = (newSize==0) ? nullptr : new ::omnetpp::simtime_t[newSize];
    size_t minSize = timestamps_arraysize < newSize ? timestamps_arraysize : newSize;
    for (size_t i = 0; i < minSize; i++)
        timestamps2[i] = this->timestamps[i];
    for (size_t i = minSize; i < newSize; i++)
        timestamps2[i] = SIMTIME_ZERO;
    delete [] this->timestamps;
    this->timestamps = timestamps2;
    timestamps_arraysize = newSize;
}

void LoRaMacFrame::setTimestamps(size_t k, ::omnetpp::simtime_t timestamps)
{
    if (k >= timestamps_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)timestamps_arraysize, (unsigned long)k);
    handleChange();
    this->timestamps[k] = timestamps;
}

void LoRaMacFrame::insertTimestamps(size_t k, ::omnetpp::simtime_t timestamps)
{
    if (k > timestamps_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)timestamps_arraysize, (unsigned long)k);
    handleChange();
    size_t newSize = timestamps_arraysize + 1;
    ::omnetpp::simtime_t *timestamps2 = new ::omnetpp::simtime_t[newSize];
    size_t i;
    for (i = 0; i < k; i++)
        timestamps2[i] = this->timestamps[i];
    timestamps2[k] = timestamps;
    for (i = k + 1; i < newSize; i++)
        timestamps2[i] = this->timestamps[i-1];
    delete [] this->timestamps;
    this->timestamps = timestamps2;
    timestamps_arraysize = newSize;
}

void LoRaMacFrame::appendTimestamps(::omnetpp::simtime_t timestamps)
{
    insertTimestamps(timestamps_arraysize, timestamps);
}

void LoRaMacFrame::eraseTimestamps(size_t k)
{
    if (k >= timestamps_arraysize) throw omnetpp::cRuntimeError("Array of size %lu indexed by %lu", (unsigned long)timestamps_arraysize, (unsigned long)k);
    handleChange();
    size_t newSize = timestamps_arraysize - 1;
    ::omnetpp::simtime_t *timestamps2 = (newSize == 0) ? nullptr : new ::omnetpp::simtime_t[newSize];
    size_t i;
    for (i = 0; i < k; i++)
        timestamps2[i] = this->timestamps[i];
    for (i = k; i < newSize; i++)
        timestamps2[i] = this->timestamps[i+1];
    delete [] this->timestamps;
    this->timestamps = timestamps2;
    timestamps_arraysize = newSize;
}

class LoRaMacFrameDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertyNames;
    enum FieldConstants {
        FIELD_transmitterAddress,
        FIELD_receiverAddress,
        FIELD_pktType,
        FIELD_sequenceNumber,
        FIELD_LoRaTP,
        FIELD_LoRaCF,
        FIELD_LoRaSF,
        FIELD_LoRaBW,
        FIELD_LoRaCR,
        FIELD_LoRaUseHeader,
        FIELD_RSSI,
        FIELD_SNIR,
        FIELD_BeaconTimer,
        FIELD_PingNb,
        FIELD_PingPeriod,
        FIELD_PingOffset,
        FIELD_BeaconTime,
        FIELD_numHop,
        FIELD_satNumber,
        FIELD_originTime,
        FIELD_groundTime,
        FIELD_route,
        FIELD_timestamps,
    };
  public:
    LoRaMacFrameDescriptor();
    virtual ~LoRaMacFrameDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyName) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyName) const override;
    virtual int getFieldArraySize(omnetpp::any_ptr object, int field) const override;
    virtual void setFieldArraySize(omnetpp::any_ptr object, int field, int size) const override;

    virtual const char *getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const override;
    virtual std::string getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const override;
    virtual omnetpp::cValue getFieldValue(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual omnetpp::any_ptr getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const override;
};

Register_ClassDescriptor(LoRaMacFrameDescriptor)

LoRaMacFrameDescriptor::LoRaMacFrameDescriptor() : omnetpp::cClassDescriptor(omnetpp::opp_typename(typeid(flora::LoRaMacFrame)), "inet::FieldsChunk")
{
    propertyNames = nullptr;
}

LoRaMacFrameDescriptor::~LoRaMacFrameDescriptor()
{
    delete[] propertyNames;
}

bool LoRaMacFrameDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<LoRaMacFrame *>(obj)!=nullptr;
}

const char **LoRaMacFrameDescriptor::getPropertyNames() const
{
    if (!propertyNames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
        const char **baseNames = base ? base->getPropertyNames() : nullptr;
        propertyNames = mergeLists(baseNames, names);
    }
    return propertyNames;
}

const char *LoRaMacFrameDescriptor::getProperty(const char *propertyName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? base->getProperty(propertyName) : nullptr;
}

int LoRaMacFrameDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? 23+base->getFieldCount() : 23;
}

unsigned int LoRaMacFrameDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeFlags(field);
        field -= base->getFieldCount();
    }
    static unsigned int fieldTypeFlags[] = {
        0,    // FIELD_transmitterAddress
        0,    // FIELD_receiverAddress
        FD_ISEDITABLE,    // FIELD_pktType
        FD_ISEDITABLE,    // FIELD_sequenceNumber
        FD_ISEDITABLE,    // FIELD_LoRaTP
        FD_ISEDITABLE,    // FIELD_LoRaCF
        FD_ISEDITABLE,    // FIELD_LoRaSF
        FD_ISEDITABLE,    // FIELD_LoRaBW
        FD_ISEDITABLE,    // FIELD_LoRaCR
        FD_ISEDITABLE,    // FIELD_LoRaUseHeader
        FD_ISEDITABLE,    // FIELD_RSSI
        FD_ISEDITABLE,    // FIELD_SNIR
        FD_ISEDITABLE,    // FIELD_BeaconTimer
        FD_ISEDITABLE,    // FIELD_PingNb
        FD_ISEDITABLE,    // FIELD_PingPeriod
        FD_ISEDITABLE,    // FIELD_PingOffset
        FD_ISEDITABLE,    // FIELD_BeaconTime
        FD_ISEDITABLE,    // FIELD_numHop
        FD_ISEDITABLE,    // FIELD_satNumber
        FD_ISEDITABLE,    // FIELD_originTime
        FD_ISEDITABLE,    // FIELD_groundTime
        FD_ISARRAY | FD_ISEDITABLE | FD_ISRESIZABLE,    // FIELD_route
        FD_ISARRAY | FD_ISEDITABLE | FD_ISRESIZABLE,    // FIELD_timestamps
    };
    return (field >= 0 && field < 23) ? fieldTypeFlags[field] : 0;
}

const char *LoRaMacFrameDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldName(field);
        field -= base->getFieldCount();
    }
    static const char *fieldNames[] = {
        "transmitterAddress",
        "receiverAddress",
        "pktType",
        "sequenceNumber",
        "LoRaTP",
        "LoRaCF",
        "LoRaSF",
        "LoRaBW",
        "LoRaCR",
        "LoRaUseHeader",
        "RSSI",
        "SNIR",
        "BeaconTimer",
        "PingNb",
        "PingPeriod",
        "PingOffset",
        "BeaconTime",
        "numHop",
        "satNumber",
        "originTime",
        "groundTime",
        "route",
        "timestamps",
    };
    return (field >= 0 && field < 23) ? fieldNames[field] : nullptr;
}

int LoRaMacFrameDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    int baseIndex = base ? base->getFieldCount() : 0;
    if (strcmp(fieldName, "transmitterAddress") == 0) return baseIndex + 0;
    if (strcmp(fieldName, "receiverAddress") == 0) return baseIndex + 1;
    if (strcmp(fieldName, "pktType") == 0) return baseIndex + 2;
    if (strcmp(fieldName, "sequenceNumber") == 0) return baseIndex + 3;
    if (strcmp(fieldName, "LoRaTP") == 0) return baseIndex + 4;
    if (strcmp(fieldName, "LoRaCF") == 0) return baseIndex + 5;
    if (strcmp(fieldName, "LoRaSF") == 0) return baseIndex + 6;
    if (strcmp(fieldName, "LoRaBW") == 0) return baseIndex + 7;
    if (strcmp(fieldName, "LoRaCR") == 0) return baseIndex + 8;
    if (strcmp(fieldName, "LoRaUseHeader") == 0) return baseIndex + 9;
    if (strcmp(fieldName, "RSSI") == 0) return baseIndex + 10;
    if (strcmp(fieldName, "SNIR") == 0) return baseIndex + 11;
    if (strcmp(fieldName, "BeaconTimer") == 0) return baseIndex + 12;
    if (strcmp(fieldName, "PingNb") == 0) return baseIndex + 13;
    if (strcmp(fieldName, "PingPeriod") == 0) return baseIndex + 14;
    if (strcmp(fieldName, "PingOffset") == 0) return baseIndex + 15;
    if (strcmp(fieldName, "BeaconTime") == 0) return baseIndex + 16;
    if (strcmp(fieldName, "numHop") == 0) return baseIndex + 17;
    if (strcmp(fieldName, "satNumber") == 0) return baseIndex + 18;
    if (strcmp(fieldName, "originTime") == 0) return baseIndex + 19;
    if (strcmp(fieldName, "groundTime") == 0) return baseIndex + 20;
    if (strcmp(fieldName, "route") == 0) return baseIndex + 21;
    if (strcmp(fieldName, "timestamps") == 0) return baseIndex + 22;
    return base ? base->findField(fieldName) : -1;
}

const char *LoRaMacFrameDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeString(field);
        field -= base->getFieldCount();
    }
    static const char *fieldTypeStrings[] = {
        "inet::MacAddress",    // FIELD_transmitterAddress
        "inet::MacAddress",    // FIELD_receiverAddress
        "int",    // FIELD_pktType
        "int",    // FIELD_sequenceNumber
        "double",    // FIELD_LoRaTP
        "inet::Hz",    // FIELD_LoRaCF
        "int",    // FIELD_LoRaSF
        "inet::Hz",    // FIELD_LoRaBW
        "int",    // FIELD_LoRaCR
        "bool",    // FIELD_LoRaUseHeader
        "double",    // FIELD_RSSI
        "double",    // FIELD_SNIR
        "int",    // FIELD_BeaconTimer
        "int",    // FIELD_PingNb
        "int",    // FIELD_PingPeriod
        "int",    // FIELD_PingOffset
        "int",    // FIELD_BeaconTime
        "int",    // FIELD_numHop
        "int",    // FIELD_satNumber
        "omnetpp::simtime_t",    // FIELD_originTime
        "omnetpp::simtime_t",    // FIELD_groundTime
        "int",    // FIELD_route
        "omnetpp::simtime_t",    // FIELD_timestamps
    };
    return (field >= 0 && field < 23) ? fieldTypeStrings[field] : nullptr;
}

const char **LoRaMacFrameDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldPropertyNames(field);
        field -= base->getFieldCount();
    }
    switch (field) {
        case FIELD_pktType: {
            static const char *names[] = { "enum", "enum",  nullptr };
            return names;
        }
        default: return nullptr;
    }
}

const char *LoRaMacFrameDescriptor::getFieldProperty(int field, const char *propertyName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldProperty(field, propertyName);
        field -= base->getFieldCount();
    }
    switch (field) {
        case FIELD_pktType:
            if (!strcmp(propertyName, "enum")) return "MacFrameType";
            if (!strcmp(propertyName, "enum")) return "flora::MacFrameType";
            return nullptr;
        default: return nullptr;
    }
}

int LoRaMacFrameDescriptor::getFieldArraySize(omnetpp::any_ptr object, int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldArraySize(object, field);
        field -= base->getFieldCount();
    }
    LoRaMacFrame *pp = omnetpp::fromAnyPtr<LoRaMacFrame>(object); (void)pp;
    switch (field) {
        case FIELD_route: return pp->getRouteArraySize();
        case FIELD_timestamps: return pp->getTimestampsArraySize();
        default: return 0;
    }
}

void LoRaMacFrameDescriptor::setFieldArraySize(omnetpp::any_ptr object, int field, int size) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldArraySize(object, field, size);
            return;
        }
        field -= base->getFieldCount();
    }
    LoRaMacFrame *pp = omnetpp::fromAnyPtr<LoRaMacFrame>(object); (void)pp;
    switch (field) {
        case FIELD_route: pp->setRouteArraySize(size); break;
        case FIELD_timestamps: pp->setTimestampsArraySize(size); break;
        default: throw omnetpp::cRuntimeError("Cannot set array size of field %d of class 'LoRaMacFrame'", field);
    }
}

const char *LoRaMacFrameDescriptor::getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldDynamicTypeString(object,field,i);
        field -= base->getFieldCount();
    }
    LoRaMacFrame *pp = omnetpp::fromAnyPtr<LoRaMacFrame>(object); (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string LoRaMacFrameDescriptor::getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValueAsString(object,field,i);
        field -= base->getFieldCount();
    }
    LoRaMacFrame *pp = omnetpp::fromAnyPtr<LoRaMacFrame>(object); (void)pp;
    switch (field) {
        case FIELD_transmitterAddress: return pp->getTransmitterAddress().str();
        case FIELD_receiverAddress: return pp->getReceiverAddress().str();
        case FIELD_pktType: return enum2string(pp->getPktType(), "flora::MacFrameType");
        case FIELD_sequenceNumber: return long2string(pp->getSequenceNumber());
        case FIELD_LoRaTP: return double2string(pp->getLoRaTP());
        case FIELD_LoRaCF: return unit2string(pp->getLoRaCF());
        case FIELD_LoRaSF: return long2string(pp->getLoRaSF());
        case FIELD_LoRaBW: return unit2string(pp->getLoRaBW());
        case FIELD_LoRaCR: return long2string(pp->getLoRaCR());
        case FIELD_LoRaUseHeader: return bool2string(pp->getLoRaUseHeader());
        case FIELD_RSSI: return double2string(pp->getRSSI());
        case FIELD_SNIR: return double2string(pp->getSNIR());
        case FIELD_BeaconTimer: return long2string(pp->getBeaconTimer());
        case FIELD_PingNb: return long2string(pp->getPingNb());
        case FIELD_PingPeriod: return long2string(pp->getPingPeriod());
        case FIELD_PingOffset: return long2string(pp->getPingOffset());
        case FIELD_BeaconTime: return long2string(pp->getBeaconTime());
        case FIELD_numHop: return long2string(pp->getNumHop());
        case FIELD_satNumber: return long2string(pp->getSatNumber());
        case FIELD_originTime: return simtime2string(pp->getOriginTime());
        case FIELD_groundTime: return simtime2string(pp->getGroundTime());
        case FIELD_route: return long2string(pp->getRoute(i));
        case FIELD_timestamps: return simtime2string(pp->getTimestamps(i));
        default: return "";
    }
}

void LoRaMacFrameDescriptor::setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValueAsString(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    LoRaMacFrame *pp = omnetpp::fromAnyPtr<LoRaMacFrame>(object); (void)pp;
    switch (field) {
        case FIELD_pktType: pp->setPktType((flora::MacFrameType)string2enum(value, "flora::MacFrameType")); break;
        case FIELD_sequenceNumber: pp->setSequenceNumber(string2long(value)); break;
        case FIELD_LoRaTP: pp->setLoRaTP(string2double(value)); break;
        case FIELD_LoRaCF: pp->setLoRaCF(Hz(string2double(value))); break;
        case FIELD_LoRaSF: pp->setLoRaSF(string2long(value)); break;
        case FIELD_LoRaBW: pp->setLoRaBW(Hz(string2double(value))); break;
        case FIELD_LoRaCR: pp->setLoRaCR(string2long(value)); break;
        case FIELD_LoRaUseHeader: pp->setLoRaUseHeader(string2bool(value)); break;
        case FIELD_RSSI: pp->setRSSI(string2double(value)); break;
        case FIELD_SNIR: pp->setSNIR(string2double(value)); break;
        case FIELD_BeaconTimer: pp->setBeaconTimer(string2long(value)); break;
        case FIELD_PingNb: pp->setPingNb(string2long(value)); break;
        case FIELD_PingPeriod: pp->setPingPeriod(string2long(value)); break;
        case FIELD_PingOffset: pp->setPingOffset(string2long(value)); break;
        case FIELD_BeaconTime: pp->setBeaconTime(string2long(value)); break;
        case FIELD_numHop: pp->setNumHop(string2long(value)); break;
        case FIELD_satNumber: pp->setSatNumber(string2long(value)); break;
        case FIELD_originTime: pp->setOriginTime(string2simtime(value)); break;
        case FIELD_groundTime: pp->setGroundTime(string2simtime(value)); break;
        case FIELD_route: pp->setRoute(i,string2long(value)); break;
        case FIELD_timestamps: pp->setTimestamps(i,string2simtime(value)); break;
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'LoRaMacFrame'", field);
    }
}

omnetpp::cValue LoRaMacFrameDescriptor::getFieldValue(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValue(object,field,i);
        field -= base->getFieldCount();
    }
    LoRaMacFrame *pp = omnetpp::fromAnyPtr<LoRaMacFrame>(object); (void)pp;
    switch (field) {
        case FIELD_transmitterAddress: return omnetpp::toAnyPtr(&pp->getTransmitterAddress()); break;
        case FIELD_receiverAddress: return omnetpp::toAnyPtr(&pp->getReceiverAddress()); break;
        case FIELD_pktType: return pp->getPktType();
        case FIELD_sequenceNumber: return pp->getSequenceNumber();
        case FIELD_LoRaTP: return pp->getLoRaTP();
        case FIELD_LoRaCF: return cValue(pp->getLoRaCF().get(),"Hz");
        case FIELD_LoRaSF: return pp->getLoRaSF();
        case FIELD_LoRaBW: return cValue(pp->getLoRaBW().get(),"Hz");
        case FIELD_LoRaCR: return pp->getLoRaCR();
        case FIELD_LoRaUseHeader: return pp->getLoRaUseHeader();
        case FIELD_RSSI: return pp->getRSSI();
        case FIELD_SNIR: return pp->getSNIR();
        case FIELD_BeaconTimer: return pp->getBeaconTimer();
        case FIELD_PingNb: return pp->getPingNb();
        case FIELD_PingPeriod: return pp->getPingPeriod();
        case FIELD_PingOffset: return pp->getPingOffset();
        case FIELD_BeaconTime: return pp->getBeaconTime();
        case FIELD_numHop: return pp->getNumHop();
        case FIELD_satNumber: return pp->getSatNumber();
        case FIELD_originTime: return pp->getOriginTime().dbl();
        case FIELD_groundTime: return pp->getGroundTime().dbl();
        case FIELD_route: return pp->getRoute(i);
        case FIELD_timestamps: return pp->getTimestamps(i).dbl();
        default: throw omnetpp::cRuntimeError("Cannot return field %d of class 'LoRaMacFrame' as cValue -- field index out of range?", field);
    }
}

void LoRaMacFrameDescriptor::setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValue(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    LoRaMacFrame *pp = omnetpp::fromAnyPtr<LoRaMacFrame>(object); (void)pp;
    switch (field) {
        case FIELD_pktType: pp->setPktType((flora::MacFrameType)value.intValue()); break;
        case FIELD_sequenceNumber: pp->setSequenceNumber(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_LoRaTP: pp->setLoRaTP(value.doubleValue()); break;
        case FIELD_LoRaCF: pp->setLoRaCF(Hz(value.doubleValueInUnit("Hz"))); break;
        case FIELD_LoRaSF: pp->setLoRaSF(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_LoRaBW: pp->setLoRaBW(Hz(value.doubleValueInUnit("Hz"))); break;
        case FIELD_LoRaCR: pp->setLoRaCR(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_LoRaUseHeader: pp->setLoRaUseHeader(value.boolValue()); break;
        case FIELD_RSSI: pp->setRSSI(value.doubleValue()); break;
        case FIELD_SNIR: pp->setSNIR(value.doubleValue()); break;
        case FIELD_BeaconTimer: pp->setBeaconTimer(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_PingNb: pp->setPingNb(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_PingPeriod: pp->setPingPeriod(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_PingOffset: pp->setPingOffset(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_BeaconTime: pp->setBeaconTime(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_numHop: pp->setNumHop(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_satNumber: pp->setSatNumber(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_originTime: pp->setOriginTime(value.doubleValue()); break;
        case FIELD_groundTime: pp->setGroundTime(value.doubleValue()); break;
        case FIELD_route: pp->setRoute(i,omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_timestamps: pp->setTimestamps(i,value.doubleValue()); break;
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'LoRaMacFrame'", field);
    }
}

const char *LoRaMacFrameDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructName(field);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    };
}

omnetpp::any_ptr LoRaMacFrameDescriptor::getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructValuePointer(object, field, i);
        field -= base->getFieldCount();
    }
    LoRaMacFrame *pp = omnetpp::fromAnyPtr<LoRaMacFrame>(object); (void)pp;
    switch (field) {
        case FIELD_transmitterAddress: return omnetpp::toAnyPtr(&pp->getTransmitterAddress()); break;
        case FIELD_receiverAddress: return omnetpp::toAnyPtr(&pp->getReceiverAddress()); break;
        default: return omnetpp::any_ptr(nullptr);
    }
}

void LoRaMacFrameDescriptor::setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldStructValuePointer(object, field, i, ptr);
            return;
        }
        field -= base->getFieldCount();
    }
    LoRaMacFrame *pp = omnetpp::fromAnyPtr<LoRaMacFrame>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'LoRaMacFrame'", field);
    }
}

}  // namespace flora

namespace omnetpp {

}  // namespace omnetpp

