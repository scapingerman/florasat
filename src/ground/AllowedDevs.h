// AllowedDevs.h
#pragma once

#include "inet/common/INETDefs.h"
#include "inet/linklayer/common/MacAddress.h"
#include <set>

namespace flora {

class AllowedDevs {
public:
    static std::set<MacAddress> getAllowedDevs() {
        std::set<MacAddress> allowedDevs;
        allowedDevs.insert(MacAddress(0x0AAA00000001ULL)); //[0]
//        allowedDevs.insert(MacAddress(0x0AAA00000002ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000003ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000004ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000005ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000006ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000007ULL));
//       allowedDevs.insert(MacAddress(0x0AAA00000008ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000009ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000000AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000000BULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000000CULL));
        allowedDevs.insert(MacAddress(0x0AAA0000000DULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000000EULL));
        allowedDevs.insert(MacAddress(0x0AAA0000000FULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000010ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000011ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000012ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000013ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000014ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000015ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000016ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000017ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000018ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000019ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000001AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000001BULL));
//        allowedDevs.insert(MacAddress(0x0AAA000001CULL));
        allowedDevs.insert(MacAddress(0x0AAA00000001DULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000001EULL));
        allowedDevs.insert(MacAddress(0x0AAA0000001FULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000020ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000021ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000022ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000023ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000024ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000025ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000026ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000027ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000028ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000029ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000002AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000002BULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000002CULL));
        allowedDevs.insert(MacAddress(0x0AAA0000002DULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000002EULL));
        allowedDevs.insert(MacAddress(0x0AAA0000002FULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000030ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000031ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000032ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000033ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000034ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000035ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000036ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000037ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000038ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000039ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000003AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000003BULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000030CULL));
        allowedDevs.insert(MacAddress(0x0AAA0000003DULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000003EULL));
        allowedDevs.insert(MacAddress(0x0AAA0000003FULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000040ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000041ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000042ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000043ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000044ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000045ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000046ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000047ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000048ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000049ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000004AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000004BULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000004CULL));
        allowedDevs.insert(MacAddress(0x0AAA0000004DULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000004EULL));
        allowedDevs.insert(MacAddress(0x0AAA0000004FULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000050ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000051ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000052ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000053ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000054ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000055ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000056ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000057ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000058ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000059ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000005AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000005BULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000005CULL));
        allowedDevs.insert(MacAddress(0x0AAA0000005DULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000005EULL));
        allowedDevs.insert(MacAddress(0x0AAA0000005FULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000060ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000061ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000062ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000063ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000064ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000065ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000066ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000067ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000068ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000069ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000006AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000006BULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000006CULL));
        allowedDevs.insert(MacAddress(0x0AAA0000006DULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000006EULL));
        allowedDevs.insert(MacAddress(0x0AAA0000006FULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000070ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000071ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000072ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000073ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000074ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000075ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000076ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000077ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000078ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000079ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000007AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000007BULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000007CULL));
        allowedDevs.insert(MacAddress(0x0AAA0000007DULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000007EULL));
        allowedDevs.insert(MacAddress(0x0AAA0000007FULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000080ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000081ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000082ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000083ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000084ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000085ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000086ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000087ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000088ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000089ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000008AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000008BULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000008CULL));
        allowedDevs.insert(MacAddress(0x0AAA0000008DULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000008EULL));
        allowedDevs.insert(MacAddress(0x0AAA0000008FULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000090ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000091ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000092ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000093ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000094ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000095ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000096ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000097ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000098ULL));
        allowedDevs.insert(MacAddress(0x0AAA00000099ULL));
//        allowedDevs.insert(MacAddress(0x0AAA00000099ULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000009AULL));
        allowedDevs.insert(MacAddress(0x0AAA0000009BULL));
//        allowedDevs.insert(MacAddress(0x0AAA0000009CULL));
        allowedDevs.insert(MacAddress(0x0AAA0000009DULL));
//        allowedDevs.insert(MacAddress(0x0AAA000009CEULL));
        allowedDevs.insert(MacAddress(0x0AAA0000009FULL));
        allowedDevs.insert(MacAddress(0x0AAA00000100ULL));

        // Agrega más direcciones MAC según sea necesario
        return allowedDevs;
    }
};

} // namespace flora
